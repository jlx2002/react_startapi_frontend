/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 09:34:52
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 19:19:18
 */
import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "./src"),
    },
  },
  server: {
    proxy: {
      "/dev": {
        target: "http://127.0.0.1:8000/api/",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/dev/, ""),
      },
      "/prod": {
        target: "https://yyapi-6243-4-1309167060.sh.run.tcloudbase.com/api/",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/prod/, ""),
      },
    },
  },
});
