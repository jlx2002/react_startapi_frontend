/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 20:09:08
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 09:36:06
 */
import { RouteObject } from "react-router-dom";
import { lazy } from "react";
import BaseLayout from "@/layouts/BaseLayout";
const Docs = lazy(() => import("../views/docs"));
const Home = lazy(() => import("../views/home"));
const NotFound = lazy(() => import("../views/404"));

// const routes: RouteObject[] = [
//   {
//     path: "/",
//     element: <Home></Home>,
//   },
//   {
//     // 动态路由和 vue-router类似
//     path: "/docs/:id",
//     element: <Docs></Docs>,
//   },
//   {
//     path: "/404",
//     element: <NotFound></NotFound>,
//   },
//   {
//     path: "*",
//     element: <NotFound></NotFound>,
//   },
// ];

const routes: RouteObject[] = [
  {
    path: "/",
    element: <BaseLayout></BaseLayout>,
    children: [
      {
        path: "/",
        element: <Home></Home>,
      },
      {
        // 动态路由和 vue-router类似
        path: "/docs/:id",
        element: <Docs></Docs>,
      },
      {
        path: "/404",
        element: <NotFound></NotFound>,
      },
      {
        path: "*",
        element: <NotFound></NotFound>,
      },
    ],
  },
];
export default routes;
