/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 19:39:26
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 18:08:12
 */
import React, { useEffect, useMemo, useState } from "react";
import { Table, Input, message } from "antd";
import styled from "./styles/home.module.scss";
import { StarFilled } from "@ant-design/icons";
const { Search } = Input;
// import { data } from "@/constant/homeData";
import type { ColumnsType } from "antd/es/table";
import { interfaceListApi } from "@/services/interface";
import { safetyInterval } from "@/utils/myDayjs";
import { useDispatch } from "react-redux";
import { loadRules } from "@/store/rules";

const columns: ColumnsType<ListItemApi> = [
  {
    title: "接口名称",
    dataIndex: "name",
    render: (text, record) => <a href={"/docs/" + record.id}>{text}</a>,
  },
  {
    title: "接口说明",
    dataIndex: "desc",
    render: (text, record) => (
      <a href={"/docs/" + record.id} className={styled.desc}>
        {text}{" "}
      </a>
    ),
  },
  {
    title: "调用次数",
    dataIndex: "count",
    render: (text, record) => (
      <a href={"/docs/" + record.id} className={styled.desc}>
        <StarFilled /> {text}
      </a>
    ),
  },
];

// FunctionComponent 函数式组件
const homePage: React.FC = () => {
  const [showData, setShowData] = useState([] as ListItemApi[]);
  const [tableData, setTableData] = useState([] as ListItemApi[]);
  const dispath = useDispatch();
  /**
   * @description: 获取展示数据
   * @return {*}
   * @author: jlx
   */
  const getShowData = async () => {
    const result = await interfaceListApi();
    if (result.data.code === 0) {
      setShowData(result.data.data);
      setTableData(result.data.data);
      dispath(loadRules());
    } else {
      message.error("很抱歉，数据找不到了！");
    }
    // console.log(safetyInterval());
  };

  /**
   * @description: 双向绑定
   * @param {string} value
   * @return {*}
   * @author: jlx
   */
  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    // console.log(e);
    let kw = e.target.value;
    setTableData(showData.filter((t) => t.desc.includes(kw)));
  };

  useEffect(() => {
    getShowData();
  }, []);
  return (
    <>
      <header className={styled.header}>
        <div className={styled["containers"]}>
          <div className={styled.title}>高速稳定免费Api接口调用平台</div>
          <div>本站现共有接口 {showData.length} 个 </div>
          <div>现本站已稳定运行{safetyInterval()} </div>
          <Search
            size="large"
            placeholder="输入api关键词"
            className={styled.search}
            onChange={onChange}
          />
        </div>
      </header>
      <main className={styled.containers}>
        <Table
          columns={columns}
          dataSource={tableData}
          rowKey={(record) => record.id}
        />
      </main>
    </>
  );
};

export default homePage;
