/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 20:07:21
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 09:37:25
 */
import React from "react";
import { Button, Result } from "antd";
// FunctionComponent 函数式组件
const notFoundPage: React.FC = () => (
  <Result
    status="404"
    title="404"
    subTitle="很抱歉，您访问的页面不存在"
    extra={
      <Button type="primary" href="/">
        返回首页
      </Button>
    }
  />
);

export default notFoundPage;
