/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 19:39:34
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-29 19:21:47
 */
import React, { useEffect, useState } from "react";
import styled from "./styles/home.module.scss";
import "./styles/docs.scss";
import { Table, Tag, List, Typography, Space, message } from "antd";
import type { ColumnsType } from "antd/es/table";
import { useParams } from "react-router-dom";
import { dataTitles } from "@/constant/docsData";
import { singleApi } from "@/services/interface";
import Debug from "@/components/Debug";

// 请求的table 列
const requsetColumns: ColumnsType<Request> = [
  {
    title: "参数名称",
    dataIndex: "params",
  },
  {
    title: "是否必填",
    dataIndex: "isRequired",
    render: (value) => (
      <Typography.Text>{value ? "是" : "否"} </Typography.Text>
    ),
  },
  {
    title: "请求参数类型",
    dataIndex: "requestType",
  },
  {
    title: "描述",
    dataIndex: "desc",
  },
];
// 返回的table 列
const responseColumns: ColumnsType<Response> = [
  {
    title: "返回字段",
    dataIndex: "retField",
  },
  {
    title: "返回值类型",
    dataIndex: "responseType",
  },
  {
    title: "描述",
    dataIndex: "desc",
  },
  {
    title: "成功响应示例",
    dataIndex: "successExample",
  },
  {
    title: "失败示例",
    dataIndex: "failExample",
  },
];

// FunctionComponent 函数式组件
const docsPage: React.FC = () => {
  // 给定默认值
  const { id = "2" } = useParams();
  // 四块内容 标题 一个列表 两个表格
  let [apiName, SetApiName] = useState("");
  let [apiDesc, SetApiDesc] = useState("");
  let [listData, SetListData] = useState([] as string[]);
  let [requestTable, SetRequestTable] = useState([] as Request[]);
  let [responseTbale, SetResponseTable] = useState([] as Response[]);
  /**
   * @description: 异步加载数据
   * @return {*}
   * @author: jlx
   */
  const loadData = async () => {
    const { data: result } = await singleApi(id);
    if (result.code != 0) {
      message.error("很抱歉，数据找不到了！");
    } else {
      let titles: string[] = [];
      dataTitles.forEach((el) => {
        titles.push(result.data[el.field]);
      });
      SetListData(titles);
      SetRequestTable(result.data.requests);
      SetResponseTable(result.data.responses);
      let { name, desc } = result.data;
      SetApiName(name), SetApiDesc(desc);
    }
  };
  useEffect(() => {
    loadData();
  }, []);
  return (
    <>
      <header className={styled.header}>
        <div className={styled["containers"]}>
          <div className={styled.title}>{apiName} </div>
          <div>{apiDesc} </div>
        </div>
      </header>
      <main className={styled.containers} style={{ textAlign: "left" }}>
        <List
          size="small"
          dataSource={listData}
          renderItem={(item, index) => (
            <List.Item>
              <Space style={{ marginBottom: "0" }}>
                <Typography.Text strong>
                  {dataTitles[index].title}
                  {"："}
                </Typography.Text>
                <Typography.Text type="secondary">{item}</Typography.Text>
              </Space>
            </List.Item>
          )}
        />
        <p className="linep">在线调试：</p>
        <Debug></Debug>
        <p className="linep">请求参数：</p>
        <Table
          size="small"
          columns={requsetColumns}
          pagination={{ position: [] }}
          dataSource={requestTable}
          rowKey={(record) => record.params}
        />
        <p className="linep">返回参数：</p>
        <Table
          size="small"
          columns={responseColumns}
          pagination={{ position: [] }}
          dataSource={responseTbale}
          rowKey={(record) => record.retField}
        />
      </main>
    </>
  );
};

export default docsPage;
