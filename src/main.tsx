/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 09:34:52
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-27 21:16:17
 */
import React from "react";
import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "antd/dist/reset.css";
import "@/assets/styles/global.scss";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  // 两种路由模式的组件： BrowserRouter(History 模式)，  HashRouter(Hash 模式)
  <BrowserRouter>
    <App />
  </BrowserRouter>
);
