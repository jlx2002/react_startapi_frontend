/*
 * @Description: 封装
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 18:46:56
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-29 16:29:38
 */
import myAxios from "@/utils/myAxios";

/**
 * @description: 获取 api 列表
 * @return {*}
 * @author: jlx
 */
export const interfaceListApi = async () => {
  return await myAxios.get<ApiListResonse>("interface/");
};

/**
 * @description: 获取单个 api
 * @param {number} id
 * @return {*}
 * @author: jlx
 */
export const singleApi = async (id: string) => {
  return await myAxios.get<SingleApiResonse>(`interface/${id}/`);
};
