/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 18:47:51
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 18:52:42
 */
import * as apis from "./interface";
import * as testApis from "./testApi";
export default {
  apis,
  testApis,
};
