/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 18:48:21
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 19:23:48
 */
import myAxios from "@/utils/myAxios";

/**
 * @description: 调试测试
 * @return {*}
 * @author: jlx
 */
export const debugTestApi = async (url: string) => {
  console.log(url);
  return await myAxios.get<ApiResponse<any>>(url.substring(1));
};
