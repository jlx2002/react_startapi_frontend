/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 21:33:08
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-29 09:49:04
 */
import React from "react";
import { Layout, FloatButton } from "antd";
// import "./baselayout.scss";
import "./components/layout.scss";
import MyFooter from "./components/MyFooter";
import MyHeader from "./components/MyHeader";
import { Outlet } from "react-router-dom";
const { Content } = Layout;

const App: React.FC = () => (
  <Layout>
    <MyHeader />
    <Content style={{ minHeight: "calc(100vh - 152px)" }}>
      <Outlet></Outlet>
    </Content>
    <MyFooter />
    <FloatButton.BackTop />
  </Layout>
);

export default App;
