/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 21:47:14
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 09:39:02
 */
import React from "react";
import { Space, Button } from "antd";

const MyHeader: React.FC = (props) => (
  <>
    <div className="header__container">
      <div className="header__left">
        <span className="header__logo">
          <Button type="link" href="/" style={{ fontSize: 18 }}>
            StartApi
          </Button>
        </span>
      </div>

      <div className="header__right">
        <Space>
          <Button type="link" href="/#/login">
            首页
          </Button>
          <Button type="link" href="/#/register">
            QQ群
          </Button>
          <Button type="link" href="/#/login">
            博客
          </Button>
        </Space>
      </div>
    </div>
  </>
);
export default MyHeader;
