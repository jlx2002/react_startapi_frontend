/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 21:47:21
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 09:40:01
 */
import React from "react";
import { Tooltip, Button } from "antd";

const MyFooter: React.FC = (props) => (
  <>
    <div className="footers">
      <div className="footer-links">
        <Tooltip title="提交issue" placement="top">
          <Button
            className="link"
            href="https://github.com/jlx2002/codepaste"
            target="_blank"
            type="link"
          >
            网站反馈
          </Button>
        </Tooltip>
        <Button type="link" className="link" href="#">
          支持项目
        </Button>
        <Tooltip title="作者邮箱地址:1125109322@qq.com" placement="top">
          <Button type="link" className="link" href="#">
            联系作者
          </Button>
        </Tooltip>
        <Button
          type="link"
          className="link"
          href="https://www.codepaste.cn"
          target="_blank"
        >
          网站友链
        </Button>
      </div>
      <div className="footer-copyright">
        <Button type="link" className="link" href="#">
          &copy;2023 StartApi&nbsp;
        </Button>
        |
        <Button
          className="link spe-link"
          type="link"
          href="https://beian.miit.gov.cn/#/Integrated/index"
        >
          豫ICP备xxxxxx号
        </Button>
      </div>
    </div>
  </>
);
export default MyFooter;
