/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-30 10:47:16
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 18:07:56
 */
import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { interfaceListApi } from "@/services/interface";
import { divideType } from "@/constant/debugData";

export const loadRules: any = createAsyncThunk(
  "urlRules/loadRules",
  async (): Promise<rules[]> => {
    const res = await interfaceListApi();
    const resultList = res.data.data;
    // 处理过后再返回
    // localhost:8000/api/bg/?lx=dongman&flat=pc 需要的信息
    // /bg/
    let rulesList: rules[] = [];
    resultList.forEach((rule) => {
      // 把前缀替换掉
      let url = rule.url?.replace("localhost:8000/api", "");
      // 找到问号部分的起始位置
      let queryStart = url?.indexOf("?");
      if (queryStart != -1) {
        url = url?.substring(queryStart || 0);
      }
      // 创建一个临时对象
      let dic: rules = {
        standardUrl: url || "none",
        // 分配预览类型
        previewType: divideType(rule.name) as previewTypes,
        name: rule.name,
        desc: rule.desc,
      };
      rulesList.push(dic);
    });
    return rulesList; // 此处的返回结果会在 .fulfilled中作为payload的值
  }
);

export const ruleSlice = createSlice({
  name: "rule",
  initialState: {
    rules: [] as rules[],
  },
  reducers: {
    SetRuleData(state, { payload }) {
      state.rules = payload;
    },
  },
  // 可以额外的触发其他slice中的数据关联改变
  extraReducers: (builder) => {
    builder.addCase(loadRules.pending, (state, action) => {
      console.log(Date.now(), "正在进行中");
    });
    builder.addCase(loadRules.fulfilled, (state, { payload }) => {
      console.log("payload", payload);
      state.rules = payload;
    });
    builder.addCase(loadRules.rejected, (state, err) => {
      console.log(err);
    });
  },
});

export const { SetRuleData } = ruleSlice.actions;

export default ruleSlice.reducer;
