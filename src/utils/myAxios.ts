/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 16:51:27
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 19:03:05
 */
import axios from "axios";

const configs = {
  baseURL: import.meta.env.VITE_BASE_URL,
  timeout: 5000,
};

const myAxios = axios.create(configs);

myAxios.defaults.headers.post["Content-Type"] =
  "application/json;charset=UTF-8"; //配置请求头
/**
 * @description: 加载中
 * @return {*}
 * @author: jlx
 */

// Add a request interceptor
myAxios.interceptors.request.use(
  function (config) {
    console.log("request", config);
    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
myAxios.interceptors.response.use(
  function (response) {
    // console.log("response", response);
    // Do something with response data
    return response;
  },
  (error) => {
    // Do something with response error
    return Promise.reject(error);
  }
);

export default myAxios;
