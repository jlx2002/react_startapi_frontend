/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 16:51:39
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 22:10:58
 */
import dayjs from "dayjs";
import "dayjs/locale/zh-cn";
import Duration from "dayjs/plugin/duration";
import isBetween from "dayjs/plugin/isBetween";
import relativeTime from "dayjs/plugin/relativeTime";
// 全局设置为中文
dayjs.locale("zh-cn");
dayjs.extend(Duration);
dayjs.extend(isBetween);
dayjs.extend(relativeTime);

/**
 * @description: 稳定运行时间
 * @return {*}
 * @author: jlx
 */
export const safetyInterval = () => {
  let differ = dayjs("2023-02-01").diff(dayjs(), "second");
  let day = Math.floor(differ / (3600 * 24));
  differ %= 3600 * 24;
  let hour = Math.floor(differ / 3600);
  differ %= 3600;
  let minute = Math.floor(differ / 60);
  return day + "天" + hour + "小时" + minute + "分";
};

export default dayjs;
