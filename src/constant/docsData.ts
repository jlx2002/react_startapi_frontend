/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-29 10:24:22
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 20:29:18
 */
type fileds = "method" | "url" | "responseType" | "example";

interface ListDataType {
  field: fileds;
  title: string;
}

export const dataTitles: ListDataType[] = [
  {
    field: "method",
    title: "请求方式",
  },
  {
    field: "url",
    title: "请求地址",
  },
  {
    field: "responseType",
    title: "返回格式",
  },
  {
    field: "example",
    title: "请求示例",
  },
];
