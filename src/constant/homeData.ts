/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 18:22:02
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-28 18:22:26
 */
export const data: ListItemApi[] = [
  {
    id: 1,
    name: "随机头像",
    desc: "获取随机头像",
    count: 0,
  },
  {
    id: 2,
    name: "随机壁纸",
    desc: "获取随机壁纸",
    count: 0,
  },
  {
    id: 3,
    name: "当前天气",
    desc: "获取当前天气",
    count: 0,
  },
];
