/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-29 18:44:17
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 19:18:52
 */
interface debugSelect {
  value: number;
  label: string;
  disabled?: boolean;
}

// 调试选择框
export const debugTypeSelect: debugSelect[] = [
  {
    label: "GET",
    value: 1,
  },
  {
    label: "POST",
    value: 2,
    disabled: true,
  },
  {
    label: "PUT",
    value: 3,
    disabled: true,
  },
  {
    label: "DELETE",
    value: 4,
    disabled: true,
  },
];

// 假数据

export const my_json_object = {
  code: 0,
  message: "0",
  ttl: 1,
  data: {
    list: [
      {
        aid: 990909915,
        videos: 1,
        tid: 21,
        tname: "日常",
        copyright: 1,
        pic: "http://i0.hdslb.com/bfs/archive/5e712811e893886fb0535a95e1609cb3ba307a39.jpg",
        label:
          "【warma爆炸电台】曾经性格阴沉的我正在分享创作心得与日常【第十一期】",
        pubdate: 1674964800,
        ctime: 1674936888,
        desc: "你好~这是我的一期电台节目，现在也已经是第十一期啦，但是即便没看过前面几期也不会影响观看，因为这是个什么都聊聊看的日常电台嘛~\n \n这期电台内容非常友好、令人安心，只是偶尔有点“问题”，但好在目前还是处于能安全观看的水平，请来看看吧。\n \n录音/视频制作：warma\n我的微博：_warma_\n \n背景插画：认知Renz\n发光粒子素材：K_Lacid \n【视频某处的曲子的staff】\n翻唱：Warma\n曲绘：不鱼\nPV：祈凉_&兽人苦工\n后期：白萝卜音乐工作室\n \n（感谢以上老师的帮助~）",
        state: 0,
        duration: 4644,
        rights: {
          bp: 0,
          elec: 0,
          download: 0,
          movie: 0,
          pay: 0,
          hd5: 0,
          no_reprint: 1,
          autoplay: 1,
          ugc_pay: 0,
          is_cooperation: 0,
          ugc_pay_preview: 0,
          no_background: 0,
          arc_pay: 0,
          pay_free_watch: 0,
        },
        owner: {
          mid: 53456,
          name: "Warma",
          face: "http://i2.hdslb.com/bfs/face/c1bbee6d255f1e7fc434e9930f0f288c8b24293a.jpg",
        },
        stat: {
          aid: 990909915,
          view: 313122,
          danmaku: 26493,
          reply: 5973,
          favorite: 30652,
          coin: 59413,
          share: 6180,
          now_rank: 0,
          his_rank: 23,
          like: 81991,
          dislike: 0,
        },
        dynamic:
          "充满惊…惊喜的电台来了！没有任何令人不安的内容，非常安全，可以绝对高兴地观看。",
        cid: 983518372,
        dimension: {
          width: 1920,
          height: 1080,
          rotate: 0,
        },
        season_id: 149941,
        short_link: "https://b23.tv/BV1Ex4y177xd",
        short_link_v2: "https://b23.tv/BV1Ex4y177xd",
        first_frame:
          "http://i1.hdslb.com/bfs/storyff/n230129qn34cqej6d9jw5w1pe9l1o7pa_firsti.jpg",
        pub_location: "云南",
        bvid: "BV1Ex4y177xd",
        season_type: 0,
        is_ogv: false,
        ogv_info: null,
        rcmd_reason: {
          content: "8万点赞",
          corner_mark: 1,
        },
      },
      {
        aid: 265905922,
        videos: 1,
        tid: 176,
        tname: "汽车生活",
        copyright: 1,
        pic: "http://i2.hdslb.com/bfs/archive/1836d11fb014f2db0c1d8b2026eda5c1aab81d08.jpg",
        label: "没想过能在国内拍到这台GT40",
        pubdate: 1674892650,
        ctime: 1674892650,
        desc: "没想过能在国内拍到这台GT40",
        state: 0,
        duration: 68,
        mission_id: 1161656,
        rights: {
          bp: 0,
          elec: 0,
          download: 0,
          movie: 0,
          pay: 0,
          hd5: 1,
          no_reprint: 1,
          autoplay: 1,
          ugc_pay: 0,
          is_cooperation: 0,
          ugc_pay_preview: 0,
          no_background: 0,
          arc_pay: 0,
          pay_free_watch: 0,
        },
        owner: {
          mid: 3493125882382878,
          name: "小商的拍车日记-",
          face: "https://i0.hdslb.com/bfs/face/9898981fdbeb10b6c0e991e29809ddfe3cd48926.jpg",
        },
        stat: {
          aid: 265905922,
          view: 755259,
          danmaku: 182,
          reply: 462,
          favorite: 6176,
          coin: 763,
          share: 854,
          now_rank: 0,
          his_rank: 0,
          like: 77144,
          dislike: 0,
        },
        dynamic: "",
        cid: 982806740,
        dimension: {
          width: 1080,
          height: 1920,
          rotate: 0,
        },
        short_link: "https://b23.tv/BV1DY411D7DX",
        short_link_v2: "https://b23.tv/BV1DY411D7DX",
        up_from_v2: 39,
        first_frame:
          "http://i0.hdslb.com/bfs/storyff/n230128a22h3nan5jpncd5yicimswkn4_firsti.jpg",
        pub_location: "广西",
        bvid: "BV1DY411D7DX",
        season_type: 0,
        is_ogv: false,
        ogv_info: null,
        rcmd_reason: {
          content: "7万点赞",
          corner_mark: 1,
        },
      },
    ],
    no_more: false,
  },
};

export const notFoundData = {
  code: 40400,
  data: "none",
  msg: "请求路径不存在",
};

export const systemErrorData = {
  code: 50000,
  data: "none",
  msg: "很抱歉，服务端出错！",
};

// 匹配对象
const matchChoice: Record<previewTypes, string[]> = {
  image: ["壁纸", "图片"],
  biliVideo: ["b站"],
  music: ["音乐", "歌曲"],
  weather: ["天气"],
  none: [""],
};

/**
 * @description: 分配类型
 * @param {string} name
 * @return {*}
 * @author: jlx
 */
export const divideType = (name: string) => {
  for (const [types, matchRule] of Object.entries(matchChoice)) {
    for (const kw of matchRule) {
      // 如果 存在匹配成功
      if (name.includes(kw)) {
        return types;
      }
    }
  }
  return "none";
};
