/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-29 09:54:29
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-29 09:54:37
 */
interface Response {
  retField: string;
  responseType: string;
  desc: string;
  successExample?: any;
  failExample?: any;
}

interface Request {
  params: string;
  isRequired: boolean;
  requestType: string;
  desc?: string;
}
