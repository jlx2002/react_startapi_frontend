type previewTypes = "image" | "music" | "biliVideo" | "weather" | "none";

interface rules {
  standardUrl: string;
  previewType: previewTypes;
  name?: string;
  desc?: string;
}
