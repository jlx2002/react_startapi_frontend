/*
 * @Description: axios 响应值封装
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-28 17:00:26
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-29 09:56:35
 */
interface ApiResponse<T> {
  code: number;
  data: T;
  msg?: string;
  errmsg?: string;
}

// api列表的每项
interface ListItemApi {
  id: number;
  name: string;
  method?: string;
  url?: string;
  desc: string;
  responseType?: string;
  example?: any;
  count: number;
}

// 单独api的详情信息
interface SingleApi {
  id: number;
  requests: Request[];
  responses: Response[];
  name: string;
  desc: string;
  method: string;
  url: string;
  responseType: string;
  example?: any;
  status: number;
  count: number;
  isDelete: boolean;
}

type ApiListResonse = ApiResponse<ListItemApi[]>;
type SingleApiResonse = ApiResponse<SingleApi>;
