/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 09:34:52
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 18:24:03
 */
import { useRoutes } from "react-router-dom";
import router from "./router";
import React from "react";
import Loading from "./components/Loading";
import { Provider } from "react-redux";
import { store } from "@/store";
import { PersistGate } from "redux-persist/integration/react";
import { persistStore } from "redux-persist";

let persistor = persistStore(store);

function App() {
  // 对路由表进行一个转换, 转换成 reactElement
  const routerView = useRoutes(router);
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <React.Suspense
          fallback={
            // 由于路由懒加载，处于加载的状态时展示的element
            <Loading></Loading>
          }
        >
          {routerView}
        </React.Suspense>
      </PersistGate>
    </Provider>
  );
}

export default App;
