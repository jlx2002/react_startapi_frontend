/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 20:59:19
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-27 21:25:51
 */
import React from "react";
import { Spin } from "antd";
import styled from "./styles/loading.module.scss";

const App: React.FC = () => (
  <div className={styled.Loading}>
    <Spin />
  </div>
);

export default App;
