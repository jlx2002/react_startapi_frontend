/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-29 17:29:14
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 22:54:39
 */
import React, { ChangeEvent, useState } from "react";
import { Input, Select, Button, Card, Tabs, message } from "antd";
import {
  debugTypeSelect,
  notFoundData,
  systemErrorData,
} from "@/constant/debugData";
import type { TabsProps } from "antd";
import ReactJson from "react-json-view";
import styled from "./styles/debug.module.scss";
import { RootState } from "@/store";
import { useSelector, useDispatch } from "react-redux";
import { debugTestApi } from "@/services/testApi";
import MyPreview from "./DebugResult/MyPreview";

const Debug: React.FC = () => {
  const rules = useSelector((state: RootState) => state.rules.rules);
  let [resJson, SetResJson] = useState({});
  let [loading, setLoading] = useState<boolean>(false);
  let [requestUrl, SetRequestUrl] = useState("");
  let [preview, SetPreview] = useState("none" as previewTypes);
  const onChange = (key: string) => {
    console.log(key);
  };

  const items: TabsProps["items"] = [
    {
      key: "1",
      label: `响应结果`,
      children: (
        <div className={styled["tabs-content"]}>
          <ReactJson src={resJson} />
        </div>
      ),
    },
    {
      key: "2",
      label: `效果预览`,
      children: (
        <div className={styled["tabs-content"]}>
          <MyPreview data={resJson} previewType={preview}></MyPreview>
        </div>
      ),
    },
  ];

  /**
   * @description: 处理 input 输入变化事件
   * @param {ChangeEvent} e
   * @return {*}
   * @author: jlx
   */
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    SetRequestUrl(e.target.value);
    // console.log(e.target.value);
  };
  /**
   * @description: 匹配api
   * @param {*} boolean
   * @return {*}
   * @author: jlx
   */
  const matchApi = (reqUrl: string): boolean => {
    for (const rule of rules) {
      if (reqUrl.startsWith(rule.standardUrl)) {
        // message.success("请求成功！");
        SetPreview(rule.previewType);
        return true;
      }
    }
    // console.log(reqUrl);
    // 弹窗，赋值（返回404的json）
    SetResJson(notFoundData);
    message.error("请求路径不存在！");
    return false;
  };

  /**
   * @description: 处理 调试按钮的点击事件
   * 按钮进行 loading 锁住，防止重复点击
   * 传入输入的路径，匹配api，如果合理再发请求，调用
   *    首页：匹配的规则在redux存储(首页拿到api列表时进行更新) setRules
   * 根据规则验证是否合理 validateUrl 不合理直接返回404数据和错误弹窗，避免刷无效请求
   * 验证成功拿到：json数据,预览数据的类型(image, video, music...)，
   * 根据url发送网络请求，更新反馈的数据
   * props 传递自定义预览组件 preview组件
   * loading状态解开
   * @return {*}
   * @author: jlx
   */
  const handleDebug = async () => {
    setLoading(true);
    // await interfaceListApi();
    const isValidate = matchApi(requestUrl);
    // 如果不合法
    if (!isValidate) {
      setLoading(false);
      return;
    }
    const result = await debugTestApi(requestUrl);
    if (result.status != 200) {
      SetResJson(systemErrorData);
      message.error("服务器出错！");
    } else {
      SetResJson(result.data);
    }
    setLoading(false);
  };
  return (
    <>
      <Input.Group compact size="large">
        <Select
          defaultValue="GET"
          size="large"
          options={debugTypeSelect}
        ></Select>
        <Input
          style={{ width: "50%" }}
          defaultValue=""
          onChange={handleChange}
        />
        <Button
          type="primary"
          size="large"
          onClick={handleDebug}
          loading={loading}
        >
          调试
        </Button>
      </Input.Group>
      <br />
      <div className={styled["tabs-container"]}>
        <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
      </div>
    </>
  );
};

export default Debug;
