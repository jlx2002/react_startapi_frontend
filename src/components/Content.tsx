/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-27 10:58:17
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-27 21:26:17
 */
import React from "react";
import styled from "./styles/content.module.scss";
// import "./content.scss";

// FunctionComponent 函数式组件
const Content: React.FC = () => {
  return (
    <div className={styled.box}>
      <p>content</p>
    </div>
  );
};

export default Content;
