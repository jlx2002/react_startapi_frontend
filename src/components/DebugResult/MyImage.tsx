/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-30 19:31:25
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-30 22:45:11
 */
import React from "react";

const MyImage: React.FC<any> = (prop) => {
  return <div>myImage data:{JSON.stringify(prop)} </div>;
};

export default MyImage;
