/*
 * @Description: 请填写文件简介
 * @Version: 0.0
 * @Autor: jlx
 * @Date: 2023-01-30 19:32:53
 * @LastEditors: jlx
 * @LastEditTime: 2023-01-31 08:34:32
 */
import React from "react";
import BiliVideo from "./BiliVideo";
import MyImage from "./MyImage";
import NeteaseMusic from "./NeteaseMusic";
import DefaultView from "./DefaultView";

interface previewProps {
  data: any;
  previewType: previewTypes;
}

const MyPreview: React.FC<previewProps> = (props) => {
  const { data, previewType: types } = props;

  /**
   * @description: 展现预览组件
   * @param {*} ReactElement
   * @return {*}
   * @author: jlx
   */
  const showPreview = (): React.ReactElement => {
    switch (types) {
      case "image":
        return <MyImage data={data} />;
      case "biliVideo":
        return <BiliVideo data={data} />;
      case "music":
        return <NeteaseMusic data={data} />;
      case "weather":
        return <DefaultView />;
      default:
        return <DefaultView />;
    }
  };
  /**
   * @description: 展现预览组件2
   * @return {*}
   * @author: jlx
   */
  const showPreviewV2 = () => {
    const previewMap: Record<previewTypes, React.ReactElement> = {
      biliVideo: <BiliVideo data={data} />,
      image: <MyImage data={data} />,
      music: <NeteaseMusic data={data} />,
      none: <DefaultView />,
      weather: <DefaultView />,
    };
    return previewMap[types];
  };

  return <>{showPreviewV2()}</>;
};

export default MyPreview;
