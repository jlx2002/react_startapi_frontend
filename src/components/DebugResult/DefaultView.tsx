import React from "react";
import { Empty } from "antd";

const DefaultView: React.FC = () => {
  return (
    <div>
      <Empty description="暂无该接口的预览信息"></Empty>
    </div>
  );
};

export default DefaultView;
